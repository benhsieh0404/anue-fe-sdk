# Anue FE SDK 
Cross project API library in `TypeScript`.

## TOC

[TOC]

## Structure

### src/packages (`@packages`)

`For application development`

Modules containing individual business logic, concerning application bundle size each package can be imported separately.

```ts
import Auth from 'anue-fe-sdk/Auth'

const auth = new Auth()

auth.comsumProviderToken(...)

```

`For development and debugging`

The package folder alias is `@packages`, packages should not coupling with each other

- [Auth](Auth)

### src/libraries (`@libraries`)

Containing modules like infrastructure and basic mechanisms, like networking, and storage accessing

- Network
- Storage

## src/utils (`@utils`)

Util functions are located here

- obfuscator 
- getty (SafeGet)

## src/constants (`@constants`)

## src/defines (automatically imported)

TypeScript definitions are located here, these files are **TYPES ONLY** 
no variable declarations like function, object, **enum**, etc.

- Auth (Auth package defines)
- Payloads (API request format defines)
- APIResponse (API response format defines)
- Network (Defines for `Network` module)

## Development

It's recommended to use VSCode as it has overall the best supports
for TypeScript development and debugging.


## Testing

```sh
$ yarn test
```

## Build

```sh
$ yarn run build
```