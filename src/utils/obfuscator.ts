const obfuscate = (input: string, key = 20): string => {
  var chars = input.split("");

  for (var i = 0; i < chars.length; i++) {
    var c = chars[i].charCodeAt(0);

    if (c <= 126) {
      chars[i] = String.fromCharCode((chars[i].charCodeAt(0) + key) % 126);
    }
  }

  return chars.join("");
};

const defuscate = (input: string): string => {
  return obfuscate(input, 106);
};

export { obfuscate, defuscate };
