/**
 * Simple implementation of null-safe get-property function, here's the rules:
 * Given o[p] if ..
 * 1. o exists returns o[p]
 * 2. returns o if o is a nullable
 * @param target Target object 
 * @param path Property path 
 * @returns value at the given path
 */

export default function getty<T = any>(target:any, path:any):T | undefined {

  return Array.isArray(path)
    ? path.reduce((p, c) => p && p[c], target)
    : target 
      ? target[path] 
      : target

}