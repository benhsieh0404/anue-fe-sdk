import getty from '@utils/getty'

describe('getty tests', () => {

  [
    [{ a: { b: { c: 1 } }}, ['a', 'b'], { c: 1 }],
    [{ a: { b: { c: 1 } }}, ['a', 'b', 'c', 'd'], undefined],
    [{ a: [1, 2, 3, 4, 5] }, ['a', 2], 3],
    [null, ['a', 2], null],
    [{ a: 1, b: 2}, null, undefined],
    [undefined, undefined, undefined],
    [undefined, undefined, undefined],
    [[1,2,3,4,5, -1, null], [2], 3],
    [{ a : { 'undefined' : { 'null' : 200 } } }, ['a', 'undefined', 'null'] , 200],
    [{ a : { 'undefined' : { 'null' : 200 } } }, ['a', undefined, null] , 200],
    [{ test : 100}, 'test', 100]
  ]
  .forEach(test => {
    it(`getty (${JSON.stringify(test[0])}, ${JSON.stringify(test[1])}) should be "${JSON.stringify(test[2])}"`, () => {
      expect(getty(test[0], test[1])).toEqual(test[2])
    })

  })

})