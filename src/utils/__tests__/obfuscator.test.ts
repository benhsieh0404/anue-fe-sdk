import { obfuscate, defuscate } from "@utils/obfuscator";

describe("obfuscator tests", () => {
  const getString = (length: number) => {
    let s = "";
    for (let i = 0; i < length; i++) {
      s += "x";
    }
    return s.replace("x", () =>
      String.fromCharCode(Math.floor(Math.random() * 128))
    );
  };

  it("should make string looks totally different", () => {
    const actual = getString(20);
    expect(obfuscate(actual) == actual).toBe(false);
  });

  it("should be reversible even with long string", () => {
    const expected = getString(300);
    const actual = obfuscate(expected);

    expect(defuscate(actual)).toBe(expected);
  });
});
