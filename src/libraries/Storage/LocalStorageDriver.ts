import StorageAbstract from './Storage.abstract'

if(typeof localStorage === 'undefined') 

throw `LocalStorageDriver is not supported in this environment, try use other drivers.`

export class LocalStorageDriver extends StorageAbstract {

  deleteItem = async (key): Promise<boolean> => {
    delete localStorage[key]
    return await true
  }

  setItemImpl = async (key: string, value: any): Promise<boolean> => {
    localStorage[key] = value
    return await true
  }

  getItemImpl = async(key: string): Promise<string> => {
    return await localStorage[key]
  }

}