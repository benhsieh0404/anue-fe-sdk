import AbstractStorageInterface from "./Storage.abstract";

// storage driver instance use localStorage as default
let storageDriver: AbstractStorageInterface

/**
 * Set another driver instance to replace the existing one.
 * Default driver is Fetch-implemented one
 * @param driver An driver which extends NetworkInterface
 */
const setDriver = (driver: AbstractStorageInterface) => {
  storageDriver = driver;
};

const getDriver = () => storageDriver;

export {
  setDriver,
  getDriver,
  AbstractStorageInterface
};
