export default abstract class StorageAbstract {

  protected abstract setItemImpl(key:string | number, value:any):Promise<boolean>;
  protected abstract getItemImpl(key:string | number):Promise<string>;

  private validateKey (key:any) {
    if(typeof key !== 'string' && typeof key !== 'number')
      throw {
        message: 'cannot setItem with a key is neither a string or number',
        detail: key
      }
  }

  /**
   * Put an item into storage entry 
   * @param key Key of the entry
   * @param value Value of the entry
   * @returns If the operation is success
   */
  setItem = async (key:string | number, value:any):Promise<boolean> => {
    this.validateKey(key)
    return await this.setItemImpl(key, value);
  }

  /**
   * Get an item from storage entry via specified key
   * @param key Key of the entry
   * @param value Value of the entry
   * @returns A promise which resolves value of the entry
   */
  getItem = async (key:string | number):Promise<string> => {
    this.validateKey(key)
    return await this.getItemImpl(key);
  }

  /**
   * Store multiple items to storage
   * @param tasks An object specify multiple key-value pairs
   */
  setMulti = async (tasks: Record<string, any>): Promise<boolean> => {

    for(const key in tasks) {
      await this.setItemImpl(key, tasks[key])
    }
    return true

  }

}
