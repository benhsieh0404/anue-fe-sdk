export default abstract class AbstractNetworkInterface {
  private static onRequestInterceptors: Anue.Network.RequestInterceptor[] = [];
  private static onResponseInterceptors: Anue.Network.ResponseInterceptor[] = [];

  protected abstract request(
    options: Anue.Network.RequestOptions
  ): Promise<Anue.Network.Response<any>>;

  /**
   * Add request interceptor that triggered before the request sent
   * @param requestOptions Original request options
   * @returns A function that remove the interceptor
   */
  static addRequestInterceptor = (fn: Anue.Network.RequestInterceptor) => {
    AbstractNetworkInterface.onRequestInterceptors.push(fn);

    return () => {
      const index = AbstractNetworkInterface.onRequestInterceptors.indexOf(fn);
      if (index > -1)
        AbstractNetworkInterface.onRequestInterceptors.splice(
          index,
          1
        );
        // @ts-ignore
        fn = null
    };
  };

  /**
   * Add response interceptor that triggered before the response resolved
   * @param requestOptions Original request options
   * @param response Original response
   * @returns A function that remove the interceptor
   */
  static addResponseInterceptor = (fn: Anue.Network.ResponseInterceptor) => {
    AbstractNetworkInterface.onResponseInterceptors.push(fn);

    return () => {
      const index = AbstractNetworkInterface.onResponseInterceptors.indexOf(fn);
      if (index > -1)
        AbstractNetworkInterface.onResponseInterceptors.splice(
          index,
          1
        );
      // @ts-ignore
      fn = null
    };
  };

  send = async<T = any> (options: Anue.Network.RequestOptions): Promise<Anue.Network.Response<T>> => {
    const {
      onRequestInterceptors,
      onResponseInterceptors
    } = AbstractNetworkInterface;

    let extendedOptions = { ...options };
    // run request interceptors
    for (const f of onRequestInterceptors) {
      extendedOptions = await f(extendedOptions);
    }

    // run response interceptors
    let extendedResult = await this.request(extendedOptions);
    for (const f of onResponseInterceptors) {
      extendedResult = await f(extendedOptions, extendedResult);
    }

    return await extendedResult;
  };
}
