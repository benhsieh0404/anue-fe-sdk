import {
  getDriver,
  setDriver,
  AbstractNetworkInterface,
  addRequestInspector,
  addResponseInspector
} from "@libraries/Network";

describe("Network interface tests", () => {
  const hash = `${Date.now()}`;
  const removeInterceptorHandles: any[] = [];

  class MockNetwork extends AbstractNetworkInterface {

    request = jest.fn().mockImplementation(
      async (options: Anue.Network.RequestOptions): Promise<Anue.Network.Response> => {
        return await {
          status: 200,
          headers: {},
          body: {
            data: {}
          },
          error: null
        };
      }
    );
  }
  const requestInterceptorMock = jest
    .fn()
    .mockImplementation(async (options: Anue.Network.RequestOptions) => {
      return await {
        ...options,
        headers: {
          ...options.headers,
          hash
        }
      };
    });
  const responseInterceptorMock = jest
    .fn()
    .mockImplementation(
      async (options: Anue.Network.RequestOptions, response: Anue.Network.Response) => {
        response.body = {
          data: hash
        };
        return await {
          ...response,
          headers: options.headers,
          requestOptions: options
        };
      }
    );

  // use mock network interface for testing
  setDriver(new MockNetwork());

  it("should be fine to send a request", async () => {
    const opts = {
      url: "mock://something.com",
      headers: { foo: "bar" }
    };
    const actual = await getDriver().send(opts);
    expect(actual).toEqual({
      status: 200,
      headers: {},
      body: {
        data: {}
      },
      error: null
    });
  });

  it("should be able to add interceptors", async () => {
    const opts = { url: "mock://foo.bar", headers: { time: `${hash}` } };

    removeInterceptorHandles.push(addRequestInspector(requestInterceptorMock));
    removeInterceptorHandles.push(
      addResponseInspector(responseInterceptorMock)
    );

    const result = await getDriver().send(opts);

    expect(result.body.data).toEqual(hash);
    expect(requestInterceptorMock).toBeCalledTimes(1);
    expect(requestInterceptorMock).toBeCalledWith(opts);
    expect(responseInterceptorMock).toBeCalledTimes(1);
    expect(result).toEqual({
      status: 200,
      headers: {
        time: hash,
        hash
      },
      requestOptions: {
        ...opts,
        headers: {
          time: hash,
          hash: hash
        }
      },
      body: {
        data: hash
      },
      error: null
    });
  });

  it("interceptors can be removed correctly", async () => {
    const opts = {
      url: "mock://something.com",
      headers: { foo: "bar" }
    };
    
    removeInterceptorHandles.forEach(f => f());
    
    const actual = await getDriver().send(opts);

    expect(actual).toEqual({
      status: 200,
      headers: {},
      body: {
        data: {}
      },
      error: null
    });
  });

});
