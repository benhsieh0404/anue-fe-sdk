import FetchNetworkDriver from "./FetchNetworkDriver";
import AbstractNetworkInterface from "./Network.abstract";

// network driver instance, we're using Fetch implemented driver as default
let networkDriver: AbstractNetworkInterface = new FetchNetworkDriver();

/**
 * Set another driver instance to replace the existing one.
 * Default driver is Fetch-implemented one
 * @param driver An driver which extends NetworkInterface
 */
const setDriver = (driver: AbstractNetworkInterface) => {
  networkDriver = driver;
};

const getDriver = () => networkDriver;
const addRequestInspector = AbstractNetworkInterface.addRequestInterceptor;
const addResponseInspector = AbstractNetworkInterface.addResponseInterceptor;

export {
  AbstractNetworkInterface,
  setDriver,
  getDriver,
  addResponseInspector,
  addRequestInspector
};
