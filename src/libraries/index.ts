import * as NetworkLib from './Network'
import * as StorageLib from './Storage'

// @ts-ignore
namespace Libraries {
  export const Network = NetworkLib
  export const Storage = StorageLib
}