import * as Network from '@libraries/Network'

class StorageMock extends Network.AbstractNetworkInterface {

  private mocks:{ test: (opt: Anue.Network.RequestOptions) => boolean, responder: () => any}[] = []

  /**
   * Mock response of specific tester, return a function which removes the mock
   * @param test A function tests the request options, use the mock if tester returns true
   * @param responder Mock responder function, can returning data or either throwing error
   * @returns Function which removes the mock
   */
  mock = (test:(opt: Anue.Network.RequestOptions) => boolean, responder: () => any) => {
    const mock = { test, responder }
    this.mocks.push(mock)
    return () => {
      this.mocks.splice(this.mocks.indexOf(mock), 1)
    }
  }

  protected request = async (options: Anue.Network.RequestOptions): Promise<Anue.Network.Response<any>> => {
    for( const mock of this.mocks) {
      if(mock.test(options)) {
        return await mock.responder()
      }
    }
    // @ts-ignore
    return await {}
  }

}

const inst = new StorageMock()

export default inst