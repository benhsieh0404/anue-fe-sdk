import * as Storage from '@libraries/Storage'

class StorageMock extends Storage.AbstractStorageInterface {
  
  storage = {}
  
  protected setItemImpl = async (key: string | number, value: any): Promise<boolean> => {
    this.storage[key] = value
    return await true
  }  
  
  protected getItemImpl = async (key: string | number): Promise<string> => {
    return await this.storage[key]
  }


}

export default new StorageMock()