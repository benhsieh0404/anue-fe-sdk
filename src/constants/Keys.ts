export default {
  Auth: {
    Storage: {
      CognitoId: 'anue.cognitoId',
      IDToken: 'anue.idToken',
      AccessToken: 'anue.accessToken',
      RefreshToken: 'anue.refreshToken',
      Authorization: 'anue.authorization'
    }
  }

}