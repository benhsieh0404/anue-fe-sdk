export enum RequestMethod {
  GET = "get",
  POST = "post",
  DELETE = "delete",
  PUT = "put",
  UPDATE = "update"
}

export default {
  RequestMethod
}