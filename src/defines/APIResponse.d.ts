declare namespace APIResponse {
  
  export interface Response<T> {
    statusCode: number,
    message: string,
    items: T
  }

  namespace v1 {
    namespace Member {
      namespace User {
        export interface CognitoOpenIdOrRegister {
          items: {
            identityId: string,
            token: string,
            newcomer: 0 | 1
          },
          message: string,
          statusCode: number
        }
      }
    }
  }
}