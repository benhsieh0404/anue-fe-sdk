declare namespace Payloads {
  namespace v1 {
    namespace Member {
      namespace User {
        export interface CognitoOpenIdOrRegister {
          type: Network.ProviderType;
          idToken?: string;
          accessToken: string;
          refresh?: string;
          channel?: Network.Channel;
        }
      }
    }
  }
}