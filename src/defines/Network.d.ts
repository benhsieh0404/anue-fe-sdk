declare namespace Anue {
  namespace Network {
    type ResponseInterceptor = (
      requestOptions: Network.RequestOptions,
      response: Network.Response
    ) => Promise<Network.Response>;

    type RequestInterceptor = (
      requestOptions: Network.RequestOptions
    ) => Promise<Network.RequestOptions>;

    interface RequestOptions {
      method?: Network.RequestMethod;
      url: string;
      headers?: Record<string, string> | undefined;
      body?: any;
    }

    interface Response<T = any> {
      status: number;
      headers: Record<string, string> | undefined | Headers;
      body: T | null;
      error: any;
    }
  }
}
