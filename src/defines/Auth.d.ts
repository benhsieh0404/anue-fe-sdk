
declare namespace Anue {
  namespace Auth {
    type ProviderType = "facebook" | "google"
  
    interface Credentials {
    authorization: string | undefined,
    accessToken: string | undefined,
    idToken: string | undefined,
    cognitoId: string | undefined
  }
  
  type SupportedChannel = 
    | "stock" 
    | "mobile-app"
    | "crypto"
    | "driver"
  
  interface ProviderToken {
    // This only required when `type` is `google`, also the token type header should be prepended
    idToken?: string,
    // Access token from provider, token type should also be prepended
    accessToken: string,
    type: ProviderType,
    channel: Channel
  }
  
  interface AuthContext {
    host: string,
    channel: SupportedChannel
  }
  }

}  


