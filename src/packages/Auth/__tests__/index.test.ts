import Endpoints from '@constants/Endpoints'
import Auth from '@packages/Auth'
import NetworkMockDriver from '@mocks/mock-network'
import StorageMockDriver from '@mocks/mock-storage'
import * as Network from '@libraries/Network'
import * as Storage from '@libraries/Storage'

const host = 'mock://auth.api'

const auth = new Auth().config({
  host,
  channel: Auth.SupportedChannel.Driver
})

beforeAll(() => {

  Network.setDriver(NetworkMockDriver)
  Storage.setDriver(StorageMockDriver)

})

describe('consumeProviderToken', () => {

  it('can get credential from provider token', async() => {
    // set request HTTP mock
    const tester = (opt) => opt.url.includes(`${host}/${Endpoints.v1.Member.User.CognitoOrRegister}`)
    const responder = () => ({
      "items": {
        "identityId": "$identityId",
        "token": "$token",
        "newcomer": 0
      },
      "message": "成功",
      "statusCode": 200
    })
    const unmock = NetworkMockDriver.mock(tester, responder)

    const credentials = await auth.consumeProviderToken({
      idToken: '$idToken',
      accessToken: '$accessToken',
      type: Auth.SupportedProviderType.Facebook,
      channel: Auth.SupportedChannel.Driver
    })

    expect(credentials).toEqual({
      "accessToken": "$accessToken",
      "authorization": "$token",
      "cognitoId": "$identityId",
      "idToken": "facebook",
    });
    unmock();

  })

  it('throws error when server does not response 200 or format is invalid', async () => {
    // set request HTTP mock
    const tester = (opt) => opt.url.includes(`${host}/${Endpoints.v1.Member.User.CognitoOrRegister}`)
    const responder = () => ({
      "items": [],
      "message": "未知的錯誤",
      "statusCode": 4008
    })
    const unmock = NetworkMockDriver.mock(tester, responder)

    try {
      await auth.consumeProviderToken({
        accessToken: '$accessToken',
        type: Auth.SupportedProviderType.Facebook,
        channel: Auth.SupportedChannel.Driver
      })

    }
    catch(error) {
      expect(error).toMatchSnapshot()
    }

    unmock();
  })

  it('throws error when connection is bad', async () => {
    // set request HTTP mock
    const tester = (opt) => opt.url.includes(`${host}/${Endpoints.v1.Member.User.CognitoOrRegister}`)
    const responder = () => {
      throw 'Bad connection'
    }
    const unmock = NetworkMockDriver.mock(tester, responder)

    try {
      await auth.consumeProviderToken({
        accessToken: '$accessToken',
        type: Auth.SupportedProviderType.Facebook,
        channel: Auth.SupportedChannel.Driver
      })

    }
    catch(error) {
      expect(error).toMatchSnapshot()
    }

    unmock();
  })

  

  it('throws error when provider type is google but id token is not specified', async () => {
    
    try {

      await auth.consumeProviderToken({
        accessToken: '$accessToken',
        type: Auth.SupportedProviderType.Google,
        channel: Auth.SupportedChannel.Driver
      })
    }
    catch(error) {
      expect(error).toMatchSnapshot()
    }
  })

})