import Misc from '@constants/Misc'
import Endpoints from '@constants/Endpoints'
import Keys from '@constants/Keys'
import * as Storage from '@libraries/Storage'
import * as Network from '@libraries/Network'
import getty from '@utils/getty'

export default class Auth {

  static SupportedProviderType: Record<string, Anue.Auth.ProviderType> = {
    Facebook : 'facebook',
    Google : 'google'
  }
  
  static SupportedChannel: Record<string, Anue.Auth.SupportedChannel> = {
    Stock : "stock" ,
    MobileApp : "mobile-app",
    Crypto : "crypto",
    Driver : "driver",
  }
  

  private idType: Anue.Auth.ProviderType = Auth.SupportedProviderType.Facebook
  private idToken: string | undefined 
  private cognitoId: string | undefined 
  private accessToken: string | undefined 
  private refreshToken: string | undefined 
  private authorization: string | undefined 
  private host = ''

  config = (authContext:Anue.Auth.AuthContext):Auth => {
    this.host = authContext.host
    return this
  }

  /**
   * Authenticate by given provider token and type, for google provider
   * the `idToken` is required.
   * @param args Provider token context object
   */
  consumeProviderToken = async (args: Anue.Auth.ProviderToken):Promise<Anue.Auth.Credentials> => {

    if(args.type === Auth.SupportedProviderType.Google && !args.idToken) {
      throw '"idToken" should not be empty when provider type is "google"'
    }

    this.idType = args.type
    this.idToken = args.idToken
    this.accessToken = args.accessToken

    return await this.renew()

  }

  /**
   * Renew authentication context automatically, performs a token refreshing under the hood.
   */
  renew = async ():Promise<Anue.Auth.Credentials> => {

    if(!this.accessToken || !this.idType)
      throw 'Anue.Auth not initialized, call "config" before use it.'

    let body:Payloads.v1.Member.User.CognitoOpenIdOrRegister = {
      type: this.idType,
      accessToken: this.accessToken,
      idToken : this.idToken || ''
    }

    const response = await Network.getDriver()
    .send<APIResponse.Response<APIResponse.v1.Member.User.CognitoOpenIdOrRegister>>({
      method: Misc.RequestMethod.POST,
      url: `${this.host}/${Endpoints.v1.Member.User.CognitoOrRegister}`,
      body
    })

    this.authorization = getty(response, ['items', 'token'])
    this.cognitoId = getty(response, ['items', 'identityId'])

    if(this.authorization && this.cognitoId) {

      const storage = Storage.getDriver()

      // optimistically set these keys into storage
      storage.setMulti({
        [Keys.Auth.Storage.CognitoId]: this.cognitoId,
        [Keys.Auth.Storage.IDToken]: this.idToken,
        [Keys.Auth.Storage.AccessToken]: this.accessToken,
        [Keys.Auth.Storage.RefreshToken]: this.refreshToken,
        [Keys.Auth.Storage.Authorization]: this.authorization
      })

    }
    else {
      throw {
        message: 'Failed to renew credentials, see detail for more information :(',
        detail: response
      }
    }

    return this.getCredentialContext()

  }

  /**
   * Get last stored credential context object
   */
  getCredentialContext = ():Anue.Auth.Credentials => {
    return {
      authorization: this.authorization,
      accessToken: this.accessToken,
      idToken: this.idType,
      cognitoId: this.cognitoId
    }
  }

}
